// -------------------------------------------------------------------
/*

Copyright (c) 2008, Rainer Blome

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

- Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.

- Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.

- Neither the name(s) of the copyright holder(s), nor the names of any
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

// -------------------------------------------------------------------
/**

TODO

o Make it work in IE without Script errors.

o Internationalize the page titles.

o Use something else that's better than document.write()?
  At least wrap it in a call like i18n.write("msg %1\$s blah").

o Prefer to generate HTML.  Then gettext() should be sufficient.

o My attempted (but under IE failing) abuse of the anchor tag (< a
  id="Transparency" >) should be replaced by uses of "span", whose
  innnerHTML can be replaced in both IE and Mozilla.

o Determine preferred content language as set in the user's
  preferences or browser's options, or use the language set by
  util.js/langselect.js.

o Is it OK to use JSON format for the translation tables or are
  there strong reasons to use PO files?

o Make it work with different character sets.
  Currently, I used HTML/entities to get non-ASCII characters,
  but this does not work with texts displayed in list boxes (why not?).

  Would utf-8 be the right way to go?
  Is it a good idea to look at the source of http://www.wikipedia.org/
  and try to learn from it?

o Specify the encoding of the .js files.
  Is this possible at all?
  Is this necessary?
  Maybe not, since it already works UTF-8-encoded i18n.js.
  Do .js files inherit their encoding from the parent document?
  Or is the browser's encoding setting used?

o Encode the .js files using UTF-8.

o Move the translation tables to separate files.

o Load only the needed translation table(s).

o For the language selection box:  Write each
  language name (the first name in each item) the way it is written in
  the language itself, in the native script of
  the language.  For example:

  Catalan
  English
  Français

  (ordered by the language-tag)

  Code or data for such a list should already exist on the web, somewhere.

o Optional: To each language selection item, add the name as
  translated to the current language. For example, when the current
  language is English, format the French item like this: "Français
  (French)".

o Mark all keys that are reused (used in more than one place).

o Internationalize further html pages and code.

o Decide whether the language selection dialog should switch the
  UI language in synchrony with the desired results language.

o Support positional message parameters.

o Create tar file that runs locally out of the box.

o Support right to left writing (direction: rtl) .

o Support top to bottom writing direction (block-progression: rl).

o Scale, localize for more languages.

DONE

o Make results page work locally.

o Internationalized: index.html, header.js, search.html, main.js,
  ui.js, hist.js, expand_ma.js.

o Determine browser's language setting. Unfortunately under my Firefox,
  the value of navigator.language is always "en-US", no matter which
  languages I have configured under Tools-> Options-> Advanced->
  General-> Languages-> Choose...

o Make element modification work on IE - using "<span id="msgkey"></span>"
  and calling xlate("msgkey") works for IE.

*/

// -------------------------------------------------------------------
/** The internationalization object.  Contains the reference to the
    current translation table (named "language"), and one translation
    table for each supported language.  */

//alert("Loading i18n...");
var i18n= {};

// -------------------------------------------------------------------
// Translation table for German

i18n.de= {
	"_LANGUAGE": "de-de",

	// header.js
	"Recent Changes": "Aktuelle Änderungen",
	"Fun": "Spaß",
	"About": "Über",
	"Help": "Hilfe",
	"Legal": "Rechtliches",
	"Invite Friends": "Freunde Einladen",

	"Sign Up / Login": "Anmelden",
	"Logout": "Abmelden",
	"Hello": "Hallo",

	"More": "Mehr",
	"more": "mehr",
	"Less": "Weniger",
	"less": "weniger",
	"Type": "Typ",
	"Edit": "Ändern",
	"Annotation": "Anmerkung",
	"Highlight": "Hervorheben",
	"Rate": "Bewerten",
	"Comment": "Kommentieren",
	"Delete": "Unterdrücken",
	"Undelete": "Wiederherstellen",
	"User": "Benutzer",
	"expand": "erweitern",

	"hide": "verbergen",
	"show": "anzeigen",

	"Poke": "Stubser",
	"nudged you": "hat Dich angestubst",
	"Friend Requests": "Freundschaft",
	"wants to be friends with you": "möchte Dein Freund sein",
	"Wall Posts": "Zettel-Nachrichten",
	"sent you": "hat Ihnen",
	"messages": "Nachrichten gesendet",
	"a message": "eine Nachricht gesendet",
	"Notifications": "Benachrichtigungen",
	"Profile": "Profil",
	"Edit Profile": "Profil Ändern",
	"Edit Photo": "Bild Ändern",
	"View Friends": "Freunde",
	"Privacy Setting": "Datenschutzeinstellungen",
	"Change Preferences": "Sonstige Einstellungen",

	"Annotate": "Anmerkung",
	"Search": "Suche",

	"Spotlight": "Hervorheben",
	"Result History": "Änderungen an diesen Ergebnissen",

	// expand_ma.js
	"Close": "Schließen",
	"Mini Article": "Mini-Artikel",
	"Full Article": "Ganze Seite",

	// index.html
	"A New Way to Search": "Eine neue Art, zu suchen",  // Could be catchier in german
	//"index_title": "Wikia Search - Eine neue Art, zu suchen",
	"Login": "Anmeldung",
	"Sign In": "Melden Sie sich an",

	"Change search! Click to make us your default browser search.":
	  "Ändern Sie die Art, zu suchen!<br/> Klicken Sie oben, um Wikia Search als Standardsuche zu verwenden.",
	"Dismiss": "Schliessen",

	"Transparency": "Transparenz",
	"Community": "Gemeinschaft",
	"Quality": "Qualität",
	"Privacy": "Datenschutz",

	"Request a wiki": "Wiki beantragen",
	"reg":
	  "Wikia&reg; ist eine eingetragene Marke von Wikia, Inc. Alle Rechte vorbehalten.",

	// login.html
	"Welcome to a different way to search, one which values":
	 "Willkommen zu einer neuen Art, zu suchen.  Ihre Werte sind: ",
	"and": "und",
	//"welcome": function(default) { }
	// "Willkommen zu einer neuen Art, zu suchen.  Ihre Werte sind: ",
	"You are logged in as": "Sie sind angemeldet als",
	"Sign In to your Account": "Melden Sie sich mit Ihrem bestehenden Konto an",
	"Want to Join?": "Sie haben noch kein Benutzerkonto?",
	"Username": "Nutzername",
	"Password": "Kennwort",
	"Sign Up": "Legen Sie ein neues an",
	"Remember me on this computer": "Auf diesem Computer dauerhaft anmelden",
	"Submit": "Abschicken",
	"Don\'t remember your password?": "Haben Sie Ihr Kennwort vergessen?",

        // search.html
	"Language": "Sprache",
	"Showing": "Angezeigt werden Ergebnisse für",
	"add suggestions": "weitere vorschlagen",
	"add": "Hinzufügen",
	"See results from": "Gleiche Suche bei",
	"add more": "hinzufügen",
	"The End": "Ende",
	"loading...": "Weitere Ergebnisse werden geholt...",
	"Add to this result": "Ergebnis hinzufügen",
	"Instantly add sites to the search results!":
	  "Hier können Sie sofort eigene Suchergebnisse hinzufügen.",
	"Click Here": "Hier Klicken",

	//main.js
	"Unable to save.  User is blocked.": "Speichern fehlgeschlagen - Nutzer ist blockiert.",
	"deleted": "unterdrückte",//"löschte",
	"undeleted": "stellte wieder her",
	"added": "fügte hinzu",
	"suggested a related result": "schlug ein weiteres Ergebnis vor",
	"spotlighted": "hob hervor",
	"rated": "bewertete",
	"star": "Stern",
	"stars": "Sternen",
	"edited": "änderte",
	"added a background": "fügte einen Hintergrund hinzu",
	"added a background image": "fügte ein Hintergrundbild hinzu",
	"commented on": "kommentierte",
	"added another place to see results": "fügte einen Siehe-auch-Eintrag hinzu",
	"annotated": "fügte eine Anmerkung hinzu",
	"annotated with a image": "fügte ein Bild hinzu",
	"annotated with a link" : "fügte einen Verweis hinzu",
	"annotated with a form" : "fügte ein Formular hinzu",
	"annotated with a text" : "fügte einen Text hinzu",
	"Show only changes by": "Zeige nur Änderungen von",
	"Site": "Webseite",
	"": "",

	"just now": "gerade eben",
	"moments ago": "vorhin",
	"day": "Tag",
	"days": "Tage",
	"hour": "Stunde",
	"hours": "Stunden",
	"ago": "her",
	"minute": "Minute",
	"minutes": "Minuten",

	"Default Language Selection": "Sprachauswahl",
	"Cancel": "Abbrechen",
	"Save": "Speichern",
	"You can select the default language for the search results.  To set a new language, just select a language from the below list and click save.":
	"Sie können hier die Sprache auswählen, für die bevorzugt Suchergebnisse angezeigt werden.  Wählen Sie dazu in der untenstehenden Liste die gewünschte Sprache aus und klicken Sie auf \"Speichern\".",
	"If would like to assist Wikia Search with its efforts to internationalize please ":
	 "Wenn Sie mithelfen möchten, Wikia Search mit mehr Sprachen auszustatten, ",
	"contact us": "schreiben Sie uns bitte",

	// recent.html
	"Edited URLs": "Geänderte URLs",
	"Spotlighted URLs": "Hervorgehobene URLs",
	"Deleted URLs": "Unterdrückte URLs",
	"Rated URLs": "Bewertete URLs",
	"Added URLs": "Hinzugefügte URLs",
	"Added Suggested Keywords": "Hinzugefügte Suchbegriffe",
	"Comments": "Kommentare",
	"Keyword Backgrounds": "Hintergründe",
	"Annotations": "Anmerkungen",
	"URL": "Webseite",
	"Sort By Date": "Sortiere nach Datum, ",
	"ascending": "aufsteigend",
	"descending": "absteigend",
	"for the result": "für das Ergebnis",
	"from the result": "für das Ergebnis",
	"to the result": "zum Ergebnis",
	"added the URL": "fügte die URL hinzu",
	"added the suggested search term": "fügte den Suchbegriff hinzu",
	"commented on the URL": "kommentierte die URL",
	"changed the background": "änderte den Hintergrund",
	"annotated the URL": "fügte eine Anmerkung hinzu",

	"loaded": "geladen",
	"Wall": "Nachricht hinterlassen",
	"Friend": "Freundschaft anbieten",
	"Block": "Blockieren",

	"": "",

// 'aa':['Afar','Afar'],
'ab':'Abchasisch',
'af':'Afrikaans',
// 'am':'Amharic',
// // FIXME: Arabic is probably running in the wrong direction
'ar':'Arabisch',
// 'as':'Assamese',
// 'ay':'Aymara',
'az':'Azerbaidschanisch',
// 'ba':'Bashkir',
'be':'Weißrussisch',
'bg':'Bulgarisch',
// 'bh':'Bihari',
// 'bpy':['ইমার ঠার/বিষ�?ণ�?প�?রিয়া মণিপ�?রী','Bishnupriya Manipuri'],
// 'bi':'Bislama',
'bn':'Bengalisch',
// 'bo':['Bosanski','Tibetan'],
'br':'Bretonisch',
'ca':'Katalanisch',
'co':'Korsisch',
'cs':'Tschechisch',
'cy':'Walisisch',
'da':'Dänisch',
'de':'Deutsch',
// 'dz':'Bhutani',
'el':'Griechisch',
'en':'Englisch',
'eo':'Esperanto',
'es':'Spanisch',
// 'et':['Eesti','Estonian'],
'eu':'Baskisch',
'fa':'Persisch (Farsi)',
'fi':'Finnisch',
'fj':'Fidschi',
// 'fo':'Faroese',
'fr':'Französisch',
'fy':'Friesisch',
'ga':'Irisch',
'gd':'Schottisch',
'gl':'Galizisch',
// 'gn':'Guarani',
// 'gu':'Gujarati',
// 'ha':'Hausa',
// // FIXME: Hebrew is possibly running in the wrong direction.
'he':'Hebräisch',
'hi':'Hindi',
'hr':'Kroatisch',
// 'hsb':'Hornjoserbsce',
'hu':'Ungarisch',
'hy':'Armenisch',
// 'hz':['Otsiherero','Herero'],
'ia':'Interlingua',
'ie':'Interlingue',
// // FIXME: id and ik were in reverse alphabetic order.
// // Is there any partiular reason why that was so?
'id':'Indonesisch',
// 'ik':'Inupiak',
// 'is':['�?slenska','Icelandic'],
// 'it':['Italiano','Italian'],
// 'iu':'Inuktitut',
// 'ja':['日本語','Japanese'],
// 'jv':'Javanese',
// // FIXME: Cannot copy and paste Georgian script
// 'ka':'Georgian',
// 'kk':'Kazakh',
// 'kl':'Greenlandic',
// 'km':'Cambodian',
// 'kn':'Kannada',
// 'ko':['한국어','Korean'],
// 'ks':'Kashmiri',
// 'ku':'Kurdish',
// 'ky':'Kirghiz',
// 'la':'Latin',
// 'ln':'Lingala',
// 'lo':'Laothian',
// 'lt':['Lietuvių','Lithuanian'],
// 'lv':'Latvian',
// 'mg':'Malagasy',
// 'mi':'Maori',
// 'mk':['Македон�?ки','Macedonian'],
// 'ml':['മലയാളം','Malayalam'],
// 'mn':'Mongolian',
// 'mo':'Moldavian',
// 'mr':'Marathi',
// 'ms':['Bahasa Melayu','Malay'],
// 'mt':'Maltese',
// 'my':'Burmese',
// 'na':'Nauru',
// 'ne':'Nepali',
// 'new':['नेपाल भाषा','Newar / Nepal Bhasa'],
// 'nl':['Nederlands','Dutch'],
// 'nn':['Norsk (nynorsk)','Norwegian (Nynorsk)'],
// 'no':['Norsk (bokmål)','Norwegian'],
// 'oc':'Occitan',
// 'om':'Afan',
// 'or':'Oriya',
// 'pa':['ਪੰਜਾਬੀ','Punjabi'],
// 'pl':['Polski','Polish'],
// 'ps':'Pashto',
// 'pt':['Português','Portuguese'],
// 'qu':'Quechua',
// 'rm':'Rhaeto',
// 'rn':'Kurundi',
// 'ro':['Română','Romanian'],
// 'ru':['Ру�?�?кий','Russian'],
// 'rw':'Kinyarwanda',
// 'sa':'Sanskrit',
// 'sd':'Sindhi',
// 'sg':'Sangho',
// 'sh':'Serbo',
// 'si':'Singhalese',
// 'sk':['Sloven�?ina','Slovak'],
// 'sl':['Slovenš�?ina','Slovenian'],
// 'sm':'Samoan',
// 'sn':'Shona',
// 'so':'Somali',
// 'sq':['Shqip','Albanian'],
// 'sr':['Срп�?ки / Srpski','Serbian'],
// 'ss':'Siswati',
// 'st':'Sesotho',
// 'su':'Sundanese',
// 'sv':['Svenska','Swedish'],
// 'sw':'Swahili',
// 'ta':['தமிழ�?','Tamil'],
// 'te':['తెల�?గ�?','Telugu'],
// 'tg':'Tajik',
// 'th':['ไทย','Thai'],
// 'ti':'Tigrinya',
// 'tk':'Turkmen',
// 'tl':'Tagalog',
// 'tn':'Setswana',
// 'to':'Tonga',
// 'tr':['Türkçe','Turkish'],
// 'ts':'Tsonga',
// 'tt':['Tatarça/Татарча','Tatar'],
// 'tw':'Twi',
// 'ug':'Uigur',
// 'uk':['Україн�?ька','Ukrainian'],
// 'ur':'Urdu',
// 'uz':'Uzbek',
// // FIXME: Either Emacs refuses to paste the doubly accented vietnamese
// // characters ("ê" with accent grave and "ê" with a dot underneath),
// // or Seamonkey refuses to properly copy them to the clipboard.
// 'vi':['Tiêng Viêt','Vietnamese'],
// 'vo':'Volapuk',
// 'wo':'Wolof',
// 'xh':'Xhosa',
// 'yi':['יידיש','Yiddish'],
// 'yo':['Yorùbá','Yoruba'],
// 'za':'Zhuang',
// 'zh':['中文','Chinese'],
// 	'zu':'Zulu',

	// Last one has no trailing comma, so that MSIE 6 / JScript can stomach it
	"DUMMY_LAST": "DUMMY_LAST"
};

// -------------------------------------------------------------------
// Translation table for French - totally incomplete and probably badly
// translated, just for testing

i18n.fr= {
	"_LANGUAGE": "fr",

	"Recent Changes": "Changements Actuelles",
	"Fun": "Plaisir",
	"About": "Sur",
	"Help": "Aide",
	"Legal": "Juridique",
	"Invite Friends": "Inviter Des Amis",

	"Sign Up / Login": "S'inscrire / Connecter",
	"Logout": "Clôturer",
	"Hello": "Salut",

	"Search": "Recherche",

	"Transparency": "Transparence",
	"Community": "Communauté",
	"Quality": "Qualité",
	"Privacy": "Protection des données",

	"Request a wiki": "Solliciter un Wiki",
	"reg": "Wikia&reg; est une marque déposée de Wikia, Inc. Tous droits réservées.",

        // search.html
	"Language": "Langue",
	//"Showing": "Angezeigt werden Ergebnisse für",
	//"add suggestions": "weitere vorschlagen",
	"add": "Ajouter",
	"See results from": "Cherchez chéz",
	"add more": "ajouter",
	"The End": "Fin",
	/*
	"loading...": "Weitere Ergebnisse werden geholt...",
	"Add to this result": "Ergebnis hinzufügen",
	"Instantly add sites to the search results!":
	  "Hier können Sie sofort eigene Suchergebnisse hinzufügen.",
	*/
	"Click Here": "Appuyez içi",

	// Last one has no trailing comma, so that MSIE can stomach it
	"DUMMY_LAST": "DUMMY_LAST"
};

// -------------------------------------------------------------------
// FIXME: How do I switch the navigator.language of my browser?

function browser_language()
{
	//return undefined != navigator.language ? navigator.language : navigator.userLanguage;
	return undefined != navigator.userLanguage ? navigator.userLanguage
	: undefined != navigator.language ? navigator.language
	: undefined != navigator.browserLanguage ? navigator.browserLanguage
	: undefined != navigator.systemLanguage ? navigator.systemLanguage
	: (alert("Could not determine language to use"), undefined);
}

// -------------------------------------------------------------------
i18n.debugtext= function()
{
	var text= "";

	text= text+
	' lang='    +navigator.language
	//for netscape flavored browsers
	//+' appName=' +navigator.appName
	//for IE
	+' usrLang=' +navigator.userLanguage
	+' brwsLang='+navigator.browserLanguage
	+' sysLang=' +navigator.systemLanguage
	;
	text= "[For debugging: Langs: Effective="+i18n.language_name
	+" &nbsp; "+text+"]";
	text= text+" Browser=: "+browser_language();

	return text;
}

// -------------------------------------------------------------------
/** Sets the global translation table (i18n.language) to the translation
    table with the given name (looked up in the i18n object). If no
    matching translation table was found, the global translation table
    is not changed.  */
/*
i18n.setlanguage= function(newlangname) {
	var newlang= i18n[newlangname];
	// If not found, try matching just the first two chars
	if (undefined == newlang) {
		newlangname= newlangname.substring(0,2);
		newlang= i18n[newlangname];
	}
	if (undefined != newlang) {
		i18n.language_name= newlangname;
		i18n.language= newlang;
	}
}*/

i18n.setlanguage= function(newlangname) {
	var newlang= i18n[newlangname];
	// If not found, try matching just the first two chars
	if (undefined == newlang) {
		newlangname= newlangname.substring(0,2).toLowerCase();
		newlang= i18n[newlangname];
	}
	if (undefined != newlang) {
		i18n.language_name= newlangname;
		i18n.language= newlang;
	}
	else i18n.language = undefined;
}

// -------------------------------------------------------------------
// param added by jeff to only display spans, etc on pages that allow changing lang
// dont need all the extra stuff if not changeable
i18n.canChange=false;
// -------------------------------------------------------------------


// -------------------------------------------------------------------
// FIXME: This should be set based on the user's preferences,
// initially indicated by the browser.

// FIXME: Allow interactive switching of language.

i18n.init= function()
{
    if (undefined == i18n.language)
    {
	var lang= getCookie("ws_lang");
	//alert("cookie lang='"+lang+"'");
	if (undefined == lang || false == lang) {
	    lang=
		//"de";
		//"de-de";
		//"fr";
		browser_language();
	}
	i18n.setlanguage(lang);
    }
    // i dont like the way i did this, but right now it works for testing
    try{if(location.href.indexOf("search.html") > -1 || location.href.indexOf("profile.html") >-1 || location.href.indexOf("index.html") >-1) {i18n.canChange = true;}else {i18n.canChange = false;}}catch(sl_ex){}
    //alert(i18n.canChange);
}
i18n.init();


// -------------------------------------------------------------------
/**  Jeff adding vars/function to translate on the fly when language is changed
	20080624
*/

var i18n_arr = new Array();

function xlateOnFly(lang) {
	for (var ii=0; ii<i18n_arr.length; ii++) {
		if (!i18n_arr[ii].i_args) {
			if(document.getElementById("i18n_"+ii)) document.getElementById("i18n_"+ii).innerHTML = gettextonfly(i18n_arr[ii].i_id, lang);
		}
		else {
			//alert(i18n_arr[ii].args.id + ":" + i18n_arr[ii].args.param);
			//if (i18n_arr[ii].i_args.id && i18n_arr[ii].i_args.param) eval("document.getElementById('" + i18n_arr[ii].i_args.id + "')." + i18n_arr[ii].i_args.param + "=gettextonfly(i18n_arr[ii].i_id, lang)");
			if (i18n_arr[ii].i_args.id && i18n_arr[ii].i_args.param) if(document.getElementById(i18n_arr[ii].i_args.id)) eval("document.getElementById('" + i18n_arr[ii].i_args.id + "')." + i18n_arr[ii].i_args.param + "='" + (i18n_arr[ii].i_args.pre ? i18n_arr[ii].i_args.pre : "")+"'+gettextonfly(i18n_arr[ii].i_id, lang)+'" + (i18n_arr[ii].i_args.post ? i18n_arr[ii].i_args.post : "")+"'");
		}
	}
}

function gettextonfly(id, lang) {
	var text= id;
	var langExists = eval("i18n." + lang);
	if (langExists) {
		text= langExists[id];
		if (undefined == text) { text= id; }
	}
	// FIXME: Format the message with the given args
	return text;
}

// -------------------------------------------------------------------

// -------------------------------------------------------------------
/** In the current translation table (value of i18n.language), looks up
    the message with the given id.

    @return the message text, if a matching message is found.
    Otherwise returns the id.

    @param id - a key identifying the message, usually the message text
    in the default language, for example English.

    FIXME: This is currently not implemented, args are ignored:
    Formats the message using the given arguments.
*/
/*
function gettext(id, args_ignored) {
	var text= id;
	if (undefined != i18n.language) {
		text= i18n.language[id];
		if (undefined == text) { text= id; }
	}
	// FIXME: Format the message with the given args
	//return text;
	//text = "<span id='i18n_" + i18n_arr.length + "'>" + text + "</span>";

	return text;
}
*/
function gettext(id, args_ignored) {
	var text= id;
	if (undefined != i18n.language) {
		text= i18n.language[id];
		if (undefined == text) { text= id; }
	}
	// FIXME: Format the message with the given args
	//return text;
	//text = "<span id='i18n_" + i18n_arr.length + "'>" + text + "</span>";

	if (i18n.canChange) {
		if (!args_ignored) {
			text = "<span id='i18n_" + i18n_arr.length + "'>" + text + "</span>";
			var id_obj = {i_id:id, i_args:false};
		}
		else var id_obj = {i_id:id, i_args:args_ignored};
		i18n_arr.push(id_obj);
	}
	return text;
}




// -------------------------------------------------------------------
/** In the current translation table, looks up the message with the
    given id.  If one is found, replaces the content of the element
    with the given id with the translated message.

    FIXME: This is currently not implemented, args are ignored:
    Formats the message using the given arguments.
*/

function xlate(id, args) {
	var text= gettext(id, args);

	// If the replacement is the same as the id, this means that
	// either there was no translation found, in which case we
	// leave the element content as it is, or that the translation is the
	// same as the id, in which case there is no need to replace
	// the content either.
	//if (id != text) { document.getElementById(id).innerHTML = text; }

	// For now, we unconditionally set the innerHTML.  This allows
	// us to leave the tag's contents empty in the HTML file.
	var elt= document.getElementById(id);
	if (undefined == elt) { alert("Could not find element with id '"+id+"'."); }
	else { elt.innerHTML = text; }
}

// -------------------------------------------------------------------
/*
Other ways of changing document text to check out:
var form_elements = document.getElementById(which_div).getElementsByTagName("textarea");
document.getElementById("json-script-output").innerHTML =
 "<div id=\"json-error-message\">" + json_profile.message + "</div>";
*/

// -------------------------------------------------------------------
// EOF
